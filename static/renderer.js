const Renderer = (function(canvas) {
    'use strict';
    const YELLOW = "#ffc927";
    const RED = "#88171b";
    const HEAD_RED = "#cc4455";
    const LIME_GREEN = "rgba(200, 200, 100, 0.5)";
    const GRID_GAP = 2;
    const body = document.querySelector('body')
        , high_score_div = document.querySelector('#high_score');
    const ctx = canvas.getContext('2d');
    const window_layout = { last_width: -1, last_height: -1, };
    let snakes = [];
    let player_color = '';
    let width = 0, height = 0;
    let next_fruit = undefined;
    let square_size = 0;
    let high_score = -1
        , last_length = -1
        , last_was_new_highscore = false;

    //setup_renderloop();

    function set_wide_layout() {
        // leave the DOM alone most of the time -> better performance
        if (window_layout.last_width === window.innerWidth
            && window_layout.last_height === window.innerHeight) {
                return;
        }
        window_layout.last_width = window.innerWidth;
        window_layout.last_height = window.innerHeight;

        if (window.innerWidth * 0.8 > window.innerHeight) {
            body.classList.add('wide-layout');
        } else {
            body.classList.remove('wide-layout');
        }
    }

    function repaint() {
        if (next_fruit === undefined) { return }

        set_wide_layout();

        let c_width = Math.max( canvas.offsetWidth, window.innerWidth );// - (2 * width);
        square_size = c_width / width;
        let c_height = square_size * height + GRID_GAP;

        //*
        while (c_height > window.innerHeight * 0.8) {
            c_width -= 20;
            square_size = c_width / width;
            c_height = square_size * height + GRID_GAP;
        }// */

        canvas.width = c_width;
        canvas.height = c_height;

        ctx.fillStyle = LIME_GREEN;

        for (let x = 0; x < width; ++x) {
            for (let y = 0; y < height; ++y) {
                paint_floor_square(x, y);
            }
        }

        paint_fruit();
        paint_control_indicators();
        paint_snakes();
    }

    function paint_floor_square(x, y) {
        const xoff = x * square_size, yoff = y * square_size;
        ctx.fillStyle = LIME_GREEN;
        ctx.fillRect(xoff + GRID_GAP/2, yoff + GRID_GAP/2,
            square_size - GRID_GAP, square_size - GRID_GAP);
    }

    function paint_fruit() {
        ctx.save();
        ctx.lineWidth = square_size / 4;
        ctx.strokeStyle = YELLOW;
        
        paint_floor_square(next_fruit.x, next_fruit.y);
        paint_floor_square(next_fruit.x, next_fruit.y);
        
        ctx.beginPath();
        ctx.arc(
            square_size * next_fruit.x + square_size/2,
            square_size * next_fruit.y + square_size/2,
            square_size / 3, 
            0, 2 * Math.PI);
        ctx.fillStyle = RED;
        ctx.fill();
        ctx.stroke();

        ctx.restore();
    }

    function paint_snakes() {
        ctx.save();
        
        for (const snake of snakes) {
            const {color, body, heading} = snake;
            
            //console.log(["snake head", body[0].x, body[0].y]);
            paint_snake(color, body, heading);
        }

        ctx.restore();
    }
    function paint_snake(color, body, heading) {
        const ss2 = square_size / 2;
        const is_player_snake = color === player_color;
        const line_width = (is_player_snake ? square_size * 3 / 4 : ctx.lineWidth = square_size * 3 / 5);
        const lw2 = line_width / 2;

        let last = undefined;

        ctx.lineWidth = line_width;

        for (let i = 0; i < body.length; ++i) {
            const p = body[i];
            const x = p.x * square_size + ss2
                , y = p.y * square_size + ss2;
            
            if (i === 0) {
                // Draw head
                ctx.strokeStyle = HEAD_RED;
                ctx.fillStyle = HEAD_RED;
                ctx.beginPath();
                if (is_player_snake) {
                    ctx.save();
                    ctx.translate(x, y);
                    ctx.rotate(orientation_to_angle(heading));

                    // Draw triangle head
                    ctx.moveTo(0      , 0 - ss2);
                    ctx.lineTo(0 - lw2, 0);
                    ctx.lineTo(0 + lw2, 0);
                    ctx.lineTo(0      , 0 - ss2);
                    ctx.fill();

                    ctx.restore();
                } else {
                    ctx.arc(x, y, lw2, 0, 2 * Math.PI);
                    ctx.fill();
                }

                ctx.strokeStyle = color;
                ctx.beginPath();
                ctx.moveTo(x, y);
            } else {
                // render wrap around
                if (delta(p, last) > 1) {
                    //console.log(["wrap", p.x, p.y, '<-', last.x, last.y]);
                    const inflow = {x: p.x, y: p.y,};
                    const outflow = {x: last.x, y: last.y,};
                    if (p.x === last.x) {
                        if (p.y > last.y) {
                            inflow.y = p.y + 1;
                            outflow.y = -1;
                        } else {
                            inflow.y = -1;
                            outflow.y = last.y + 1;
                        }
                    } else {
                        if (p.x > last.x) {
                            inflow.x = p.x + 1;
                            outflow.x = -1;
                        } else {
                            inflow.x = -1;
                            outflow.x = last.x + 1;
                        }
                    }
                    ctx.lineTo(outflow.x * square_size + square_size / 2
                        , outflow.y * square_size + square_size / 2);
                    ctx.stroke();

                    ctx.beginPath();
                    ctx.moveTo(inflow.x * square_size + square_size / 2
                            , inflow.y * square_size + square_size / 2);
                }


                ctx.lineTo(x, y);
                if (i === body.length - 1) {
                    ctx.stroke();

                    // Draw tail
                    ctx.save();
                    ctx.beginPath();
                    ctx.fillStyle = color;
                    ctx.arc(x, y, ctx.lineWidth/2, 0, 2 * Math.PI);
                    ctx.fill();
                    ctx.restore();
                }
            }

            last = p;
        }
    }

    function paint_control_indicators() {
        if (player_color === '') { return }
        const snake = snakes.find(si => si.color === player_color);
        const ss2 = square_size / 2;
        const line_width = square_size * 3 / 4;
        const p = snake.body[0];
        const x = p.x * square_size + ss2
            , y = p.y * square_size + ss2;

        ctx.save();
        ctx.translate(x, y);
        ctx.rotate(orientation_to_angle(snake.heading));

        // Draw the rotation indicators
        ctx.beginPath()
        ctx.fillStyle = '#fff';
        ctx.strokeStyle = '#fff';
        ctx.font = `${Math.floor(line_width)}px Tinos`;
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';

        ctx.fillText("↑", 0, -square_size);

        ctx.font = `${Math.floor(line_width * 0.7)}px Tinos`;
        ctx.fillText("↰", -square_size, 0);
        ctx.fillText("↱", +square_size, 0);

        ctx.restore();
    }

    const timeout = 1000/30;
    function setup_renderloop() {
        requestAnimationFrame(_ => {
            const start = Date.now();
            repaint();
            const now = Date.now();

            const next = Math.max(0, timeout - (now - start));
            //console.log(next);
            setTimeout(setup_renderloop, next);
        })
    }

    function delta(p, q) {
        if (typeof q === 'undefined') {
            return 0;
        }
        return Math.abs(p.x - q.x) + Math.abs(p.y - q.y);
    }

    function orientation_to_angle(heading) {
        switch (heading) {
            case "North":
                return 0;
            case "South":
                return Math.PI;
            case "West":
                return Math.PI / -2;
            case "East":
                return Math.PI / 2;

            default:
                throw "invalid heading:" + heading;
        }
    }

    return {
        msg: function (command, data) {
            switch (command) {
                case "UpdateFieldInfo":
                    width = data.width;
                    height = data.height;
                    next_fruit = data.next_fruit;

                    repaint();
                    //console.log("UpdateFieldInfo");
                    break;
                
                case "UpdateSnakes":
                    snakes = data;
                    //console.log(["UpdateSnakes", snakes]);
                    repaint();

                    const my_snake = snakes.filter(s => s.color === player_color)[0];
                    const new_length = my_snake ? my_snake.body.length : 0;
                    if (new_length > high_score) {
                        high_score = new_length;

                        last_was_new_highscore = true;
                    } else {
                        if (last_length === new_length) {
                            /* still playing */
                        } else {
                            /* smaller */
                            last_was_new_highscore = false;
                        }
                    }
                    last_length = new_length;
                    let html = `High Score: ${high_score}`
                    if (last_was_new_highscore) {
                        html = `<b>🎉New ${html}🎉</b>`;
                    }
                    
                    high_score_div.innerHTML = `${html}<br>Current score: ${new_length}`;
                    break;

                case "UpdateControllColor":
                    player_color = data;
                    break;
                
                default:
                    console.error("unimlemented command", command);
                    break;
            }
        }
    }
})(document.querySelector('canvas'));

