const Buttons = (function buttons() {
    'use strict';

    const left = document.querySelector('button#left');
    const straight_up = document.querySelector('button#straight_up');
    const right = document.querySelector('button#right');
    const down = document.querySelector('button#down');
    const buttons_background = left.parentNode;
    const CONTROLL_MODE_NOT_SELECTED = 0;
    const CONTROLL_MODE_ROTATION = 1;
    const CONTROLL_MODE_D_PAD = 2;

    let buttons_background_color = '';
    let heading = undefined;
    let controll_mode = CONTROLL_MODE_NOT_SELECTED;

    const button_config = {
        'left': { elem: left, enabled: false, },
        'up': { elem: straight_up, enabled: false, },
        'right': { elem: right, enabled: false, },
        'down': { elem: down, enabled: false, },
    };

    Object.entries(button_config).forEach(([name, config], _) => {
        //console.log(name);
        config.elem.addEventListener('click', mk_button_handler(name));
    });

    update_buttons();

    document.addEventListener('keydown', handle_keydown);

    function setup(mode) {
        switch (mode) {
            case CONTROLL_MODE_ROTATION:
                button_config.down.elem.classList.add('hidden');

                button_config.up.enabled = true;
                button_config.up.elem.innerHTML = '↑<span class="hide-when-small"> straight ↑</span>';

                button_config.left.enabled = true;
                button_config.left.elem.innerHTML = '↰<span class="hide-when-small"> left</span>';

                button_config.right.enabled = true;
                button_config.right.elem.innerHTML = '<span class="hide-when-small">right </span>↱';

                break;
            
            case CONTROLL_MODE_D_PAD:
                button_config.down.elem.classList.remove('hidden');

                button_config.up.elem.innerHTML = '↑<span class="hide-when-small"> up ↑</span>';
                button_config.down.elem.innerHTML = '↓<span class="hide-when-small"> down ↓</span>';
                button_config.left.elem.innerHTML = '←<span class="hide-when-small"> left</span>';
                button_config.right.elem.innerHTML = '<span class="hide-when-small">right </span>→';

                // wait for the snake events to tell us the orentation
                break;

            case CONTROLL_MODE_NOT_SELECTED:
            default:
                throw "Buttons::setup invalid controll_mode: " + mode;
        }

        controll_mode = mode;
        update_buttons();

        straight_up.focus();
    }

    function mk_button_handler(name) {
        return function() {
            //console.log("sending", action);
            this.focus();
            if (controll_mode === CONTROLL_MODE_ROTATION) {
                handle_button(name);
            } else {
                handle_button(translate_button(name));
            }
        }
    }

    function update_buttons() {
        switch (controll_mode) {
            case CONTROLL_MODE_NOT_SELECTED:
                break;

            case CONTROLL_MODE_ROTATION:
                button_config.left.enabled = true;
                button_config.up.enabled = true;
                button_config.right.enabled = true;
                break;
        
            case CONTROLL_MODE_D_PAD:
                button_config.left.enabled = true;
                button_config.up.enabled = true;
                button_config.down.enabled = true;
                button_config.right.enabled = true;

                switch (heading) {
                    case "East":
                        button_config.left.enabled = false;
                        break;
                    case "South":
                        button_config.up.enabled = false;
                        break;
                    case "North":
                        button_config.down.enabled = false;
                        break;
                    case "West":
                        button_config.right.enabled = false;
                        break;
                
                    default:
                        console.error(["Buttons::update_buttons invalid heading", heading]);
                        break;
                }
                break;
            
            default:
                //throw new Error("Buttons::update_buttons");
                throw "Buttons::update_buttons";
        }


        Object.entries(button_config)
            .forEach(([name, config], _) => {
                config.elem.disabled = !config.enabled;
            });
    }

    function handle_keydown(ev) {
        const composed = ev.metaKey || ev.altKey || ev.ctrlKey;
        //console.log(['handle_keydown', ev, ev.code, ev.keyCode, composed]);
        if (ev.isComposing || composed || ev.keyCode === 229) {
            // Ignoring Combos
            return;
        }

        switch (ev.keyCode) {
            case 37:
            case 65: // A
            case 74: // J
                left.click();
                break;
            case 38:
            case 87: // W
            case 73: // I
                straight_up.click();
                break;
            case 39:
            case 68: // D
            case 76: // L
                right.click();
                break;
            case 40:
            case 83: // S
            case 75: // K
                down.click();
                break;
        
            case 82: // R => Rotational controlls
                setup(CONTROLL_MODE_ROTATION);
                Main.hide_info_box();
                break;
            case 67: // C => Classic
                setup(CONTROLL_MODE_D_PAD);
                Main.hide_info_box();
                break;
        
            default:
                return;
                break;
        }
        ev && ev.preventDefault && ev.preventDefault();
    }

    function translate_button(name) {
        //console.log(["translate_button", heading, name]);
        switch (heading) {
            case "North":
                return name;
            case "East":
                return { up: "left", right: "up", down: "right" }[name];
            case "South":
                return { right: "left", down: "up", left: "right" }[name];
            case "West":
                return { down: "left", left: "up", up: "right" }[name];
        
            default:
                throw "translate_button";
        }
    }

    function handle_button(name) {
        //console.log(["handle_button", name]);
        let action = undefined;
        switch (name) {
            case "left": action = "Left"; break;
            case "up": action = "Straight"; break;
            case "right": action = "Right"; break;

            default:
                console.error(["Button::handle_button unknown button", name]);
        }
        Main.send({ "PlayerInput": action });
    }

    return {
        update_controll_color: function (color) {
            buttons_background.style.backgroundColor = color;
            buttons_background_color = color;
        },
        msg: function (command, data) {
            switch (command) {
                case "UpdateSnakes":
                    const new_heading = data.filter(e => e.color === buttons_background_color)[0].heading;
                    if (new_heading !== heading) {
                        heading = new_heading;
                        update_buttons();
                    }

                    buttons_background.focus();
                    break;

                default:
                    console.error(["Buttons::msg -> unimplemented command", cmd]);
                    break;
            }
        },
        setup: setup,
        CONTROLL_MODE_ROTATION: CONTROLL_MODE_ROTATION,
        CONTROLL_MODE_D_PAD: CONTROLL_MODE_D_PAD,
    }
})();
