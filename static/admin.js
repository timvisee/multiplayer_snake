const Admin = (function () {
    'use strict';
    const update_button = document.querySelector('#update_arena')
        , width = document.querySelector('#new_field_width')
        , height = document.querySelector('#new_field_height')
        , n_snakes = document.querySelector('#new_n_snakes');
    const update_button_text = update_button.innerHTML;

    update_button.addEventListener('click', async ev => {
        ev.preventDefault();

        update_button.innerHTML = '⏳ ♻ Working ♻ ⏳';

        const data = {
            width: 0 | width.value,
            height: 0 | height.value,
            n_snakes: 0 | n_snakes.value,
        };

        let r = undefined;
        try {
            r = await fetch('/admin/update', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json', },
                body: JSON.stringify(data),
            });
        } catch (error) {
            console.error(error);
        }

        update_button.innerHTML = `${update_button_text} ${r && r.ok ? '✔' : '✘'}`;
    })
})();