// Needed because of the imported functions we only use in the server
#![allow(dead_code)]

#[path = "../configuration.rs"]
mod configuration;

use configuration::*;

fn main() -> std::io::Result<()> {
    let mut config = restore_or_default().expect("unable to restore existing config");

    let mut args = std::env::args().skip(1);

    let action = args
        .next()
        .expect("No message supplied [list, add, remove]");
    match &*action {
        "list" => {
            println!("==== Current Admin Users ====");
            for userkey in &config.get_admin_user_keys() {
                println!("  {}", userkey);
            }
        }
        "add" => {
            let userkey = config.add_user();
            config.save()?;
            println!("added {:?}", userkey);
        }
        "remove" => {
            let argument = args.next().expect("Missing argument");

            config.remove_user(argument.trim());

            config.save()?;
        }
        invald => {
            panic!("unknown action {:?}", invald);
        }
    }

    Ok(())
}
