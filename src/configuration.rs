use log::{error, info};
use rand::Rng;
use serde::{Deserialize, Serialize};
use std::{
    path::Path,
    sync::{Arc, Mutex},
};

pub type UserData = String;
pub type Config = Arc<Mutex<ConfigRaw>>;
pub type WebConfig = actix_web::web::Data<crate::configuration::Config>;

const CONFIG_PATH: &str = "./mps_config.json";

#[derive(Debug, Serialize, Deserialize)]
pub struct ConfigRaw {
    admin_user_keys: Vec<UserData>,
    // , serialize_with = "serialize_to_base64", deserialize_with = "base64::decode",
    // https://github.com/marshallpierce/base64-serde/commit/67eeacd7b2e7031fe81211823b756743cca427cb
    #[serde(default)]
    cookie_private_key: Vec<u8>,
}

impl ConfigRaw {
    pub fn save(&mut self) -> std::io::Result<()> {
        let data = serde_json::to_string_pretty(&self)?;

        std::fs::write(CONFIG_PATH, data)
    }

    #[allow(dead_code)]
    pub fn get_admin_user_keys(&self) -> Vec<UserData> {
        self.admin_user_keys.clone()
    }

    pub fn get_user_data(&mut self, userkey: &str) -> Option<&mut UserData> {
        //debug!("get_user_data({})", userkey);
        let userkey = userkey.trim();
        self.admin_user_keys
            .iter_mut()
            .filter(|uk| *uk == userkey)
            .next()
    }

    pub fn get_private_key(&self) -> Vec<u8> {
        self.cookie_private_key.clone()
    }

    pub fn add_user(&mut self /*permissions: UserPermission*/) -> String {
        let userkey = base64::encode(gen_private_key());

        self.admin_user_keys.push(userkey.clone());

        userkey
    }

    #[allow(dead_code)]
    pub fn remove_user(&mut self, userkey: &str) -> usize {
        let org = self.admin_user_keys.len();

        self.admin_user_keys.retain(|u| u != userkey);

        org - self.admin_user_keys.len()
    }
}

fn gen_private_key() -> Vec<u8> {
    let mut private_key = vec![0u8; 32];

    // Generate a random 32 byte key. Note that it is important to use a unique
    // private key for every project. Anyone with access to the key can generate
    // authentication cookies for any user!
    rand::thread_rng().fill(&mut *private_key);

    private_key
}

pub fn restore_or_default() -> std::io::Result<ConfigRaw> {
    let path = Path::new(CONFIG_PATH);
    let mut save = false;
    let mut config: ConfigRaw;

    if path.exists() && path.is_file() {
        let data = std::fs::read_to_string(path)?;
        config = serde_json::from_str(&*data)?;

        info!("Restored configuration from {}", path.display());

        if config.cookie_private_key.len() < 32 {
            error!("cookie_private_key is to short! Regenerating");
            config.cookie_private_key = gen_private_key();
            save = true;
        }
    } else {
        info!("No configuration found, loading default");
        config = ConfigRaw {
            admin_user_keys: vec![],
            cookie_private_key: gen_private_key(),
        };
        save = true;
    }

    if config.admin_user_keys.is_empty() {
        let userkey = config.add_user();
        error!("No Admin Users found! Added {:?}", userkey);
        save = true;
    }

    config
        .admin_user_keys
        .iter()
        //.filter(|u| u.is_admin())
        .for_each(|u| info!("Admin User: {}", u));

    if save {
        config.save()?;
    }
    Ok(config)
}
