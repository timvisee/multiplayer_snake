#![allow(unused_imports)]

use actix::{
    dev::channel::{channel, AddressSender},
    fut,
    prelude::*,
    utils::TimerFunc,
    Actor, Addr, AsyncContext, Context, Handler, Message, MessageResult, SpawnHandle,
    StreamHandler,
};
use actix_files::NamedFile;
use actix_identity::{CookieIdentityPolicy, Identity, IdentityService};
use actix_web::{
    error, get, post, web, App, Error, HttpRequest, HttpResponse, HttpServer, Responder,
};
use actix_web_actors::ws::{self, WebsocketContext};
use defaultmap::DefaultHashMap;
use log::{debug, error, info, warn};
use rand::{seq::SliceRandom, Rng};
use serde::{Deserialize, Serialize};
use std::{
    collections::{BTreeMap, HashSet},
    path::PathBuf,
    time::{Duration, Instant},
};
use tera::Tera;

const TICK_INTERVAL: Duration = Duration::from_secs(1);
const N_ROUNDS_BEFORE_IDLE_PLAYER: u32 = 16;
const RANDOM_ACTION_CANCHE_FOR_INACTIVE_SNAKES: f64 = 0.1;
const PING_PONG_DATA: [u8; 1] = [0x42];
const COLORS: [&'static str; 6] = [
    "#0014ff",
    "#ff1400",
    "#148800",
    "rgb(102, 186, 197)",
    // too dark for RustFest branding "rgb(71, 78, 90)",
    "rgb(249, 176, 139)",
    "rgb(120, 51, 225)",
];
const MAX_WEIGHT: usize = 1000_000_000;

type SnakeColor = String;

#[derive(Message)]
#[rtype(result = "()")]
pub struct CreateSnake {
    pub body: Vec<Pos2d>,
    pub heading: Heading,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct RemoveSnake;

#[derive(Message)]
#[rtype(result = "()")]
struct SubscribeMyWs(Addr<MyWs>);
#[derive(Message, Clone)]
#[rtype(result = "()")]
struct UnsubscribeMyWs(Addr<MyWs>);
/*#[derive(Message)]
#[rtype(result = "()")]
struct AssignToRandomSnake(Addr<MyWs>);*/

#[derive(Message)]
#[rtype(result = "()")]
struct SubscribeSnake {
    snake: SnakeAddr,
    color: SnakeColor,
}

struct ExecuteTick {
    collector: Addr<SnakesNextStepCollector>,
}
impl Message for ExecuteTick {
    type Result = ();
}
#[derive(Message)]
#[rtype(result = "()")]
struct CollectedSnakeBodies(Vec<SnakeBody>);

enum MaybePlayerInput {
    None { for_n_rounds: u32 },
    Some(PlayerInput),
}
impl Message for MaybePlayerInput {
    type Result = ();
}
#[derive(Message)]
#[rtype(result = "()")]
struct CollectedDirection {
    input: PlayerInput,
    arena_collector: Addr<SnakesNextStepCollector>,
}

#[derive(Debug, Message)]
#[rtype(result = "()")]
pub struct ResetAt(Pos2d);
#[derive(Debug, Message)]
#[rtype(result = "()")]
pub struct OverwriteHead(Pos2d);

#[derive(Message)]
#[rtype(result = "()")]
pub struct GetCurrentUserAction(Addr<DirectionCollector>);
#[derive(Message)]
#[rtype(result = "()")]
pub struct ClipTail;

#[derive(Message)]
#[rtype(result = "ArenaConfig")]
pub struct GetArenaConfig;
#[derive(Message, Clone, Deserialize)]
#[rtype(result = "ArenaConfig")]
pub struct UpdateArenaConfig {
    pub width: isize,
    pub height: isize,
    pub n_snakes: usize,
}
#[derive(Message, MessageResponse, Clone, Serialize)]
#[rtype(result = "()")]
pub struct ArenaConfig {
    pub width: isize,
    pub height: isize,
    pub n_snakes: usize,
    //pub max_ping: Duration,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct SubscribeStats(Addr<Stats>);
#[derive(Message)]
#[rtype(result = "()")]
pub struct SubscribeArena(Addr<Arena>);
#[derive(Message)]
#[rtype(result = "Metrics")]
pub struct GetMetrics;
#[derive(MessageResponse)]
pub struct Metrics(pub String);
#[derive(Message)]
#[rtype(result = "()")]
pub struct StatsWebsocketUpgrade;
#[derive(Message)]
#[rtype(result = "()")]
pub struct StatsNoWebsocketUpgrade;
#[derive(Message)]
#[rtype(result = "()")]
pub struct UpdateSnakeStats {
    color: SnakeColor,
    n_players: usize,
    length: usize,
}
#[derive(Message)]
#[rtype(result = "()")]
pub struct RemoveSnakeStats {
    color: SnakeColor,
}

#[derive(Message)]
#[rtype(result = "()")]
pub enum CrashRule {
    /// All Snakes involved die
    AllDie,
    /// The one ramming into the other dies (Tron like)
    CrasherDies,
    /// Ram others to win
    CrashedDies,
}

// ----- Network able structures

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(tag = "PlayerInput")]
pub enum PlayerInput {
    Left,
    Straight,
    Right,
}

#[derive(Debug, Serialize)]
pub enum ClientCommand {
    UpdateFieldInfo(UpdateFieldInfo),
    UpdatePlayerCount(UpdatePlayerCount),
    UpdateSnakes(SnakeBodies),
    UpdateControllColor(SnakeColor),
    PingTime(PingTime),
    InvalidInput(String),
}
#[derive(Debug, Message, Serialize)]
#[rtype(result = "()")]
pub struct UpdatePlayerCount(usize);
impl UpdatePlayerCount {
    pub fn new(d: usize) -> Self {
        Self(d)
    }
}
#[derive(Debug, Message, Serialize)]
#[rtype(result = "()")]
pub struct UpdateFieldInfo {
    width: isize,
    height: isize,
    next_fruit: Pos2d,
}
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize)]
pub struct Pos2d {
    pub x: isize,
    pub y: isize,
}

// ----- Internal Structure
pub struct SnakeInfo {
    addr: SnakeAddr,
    n_subscribers: usize,
    body: SnakeBody,
}
#[derive(Message)]
#[rtype(result = "()")]
pub struct UpdatePlayerCountOfSnake {
    n_subscribers: usize,
    color: SnakeColor,
}
// ----- END

#[derive(Debug, Message, Serialize)]
#[rtype(result = "()")]
pub struct SnakeBodies(Vec<SnakeBody>);

#[derive(Debug, Clone, Message, Serialize)]
#[rtype(result = "()")]
pub struct SnakeBody {
    heading: Heading,
    color: SnakeColor,
    body: Vec<Pos2d>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize)]
pub enum Heading {
    West,
    North,
    East,
    South,
}
use Heading::*;
impl Heading {
    fn random() -> Heading {
        let mut rng = rand::thread_rng();
        [West, North, East, South]
            .choose(&mut rng)
            .map(|d| *d)
            .unwrap_or(North)
    }
}

#[derive(Debug, Serialize)]
pub enum PingTime {
    Ping { ms: usize },
    NetworkTooSlow { ms: usize },
}

// ----------- Internal Actors
pub struct Stats {
    auto_scalar: Addr<AutoScaler>,
    snake_players: BTreeMap<SnakeColor, (Box<prometheus::Gauge>, Box<prometheus::Gauge>)>,
    prometheus: prometheus::Registry,
    active_players: prometheus::Gauge,
    n_snakes: prometheus::Gauge,
    websocket_upgrade: prometheus::Counter,
    no_websocket_upgrade: prometheus::Counter,
}
impl Stats {
    pub fn new(auto_scalar: &Addr<AutoScaler>) -> Addr<Stats> {
        use prometheus::{
            Counter, CounterVec, Encoder, Gauge, GaugeVec, Opts, Registry, TextEncoder,
        };
        let prometheus = Registry::new();

        let active_players = Gauge::new(
            "multiplayersnake_active_players",
            "amount of currently connected clients",
        )
        .unwrap();
        prometheus
            .register(Box::new(active_players.clone()))
            .unwrap();

        // Maybe use the labels in the future to distiguish the snakes
        let gauge_opts = Opts::new(
            "multiplayersnake_amount_of_snakes",
            "how many Snakes are in the Arena",
        )
        .const_label("okay", "5")
        .const_label("max", "8");
        let n_snakes = Gauge::with_opts(gauge_opts).unwrap();
        prometheus.register(Box::new(n_snakes.clone())).unwrap();

        let websocket_upgrade = Counter::new(
            "multiplayersnake_websocket_upgrade",
            "report how many clients suceeded connecting",
        )
        .unwrap();
        prometheus
            .register(Box::new(websocket_upgrade.clone()))
            .unwrap();
        let no_websocket_upgrade = Counter::new(
            "multiplayersnake_no_websocket_upgrade",
            "report how many clients failed to connect",
        )
        .unwrap();
        prometheus
            .register(Box::new(no_websocket_upgrade.clone()))
            .unwrap();

        Stats {
            auto_scalar: auto_scalar.clone(),
            snake_players: BTreeMap::new(),
            prometheus,
            active_players,
            websocket_upgrade,
            no_websocket_upgrade,
            n_snakes,
        }
        .start()
    }
}
impl Actor for Stats {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.auto_scalar.do_send(SubscribeStats(ctx.address()));
    }
}
/*impl Handler<CreateSnake> for Stats {
    type Result = ();

    fn handle(&mut self, _msg: CreateSnake, _ctx: &mut Context<Self>) -> Self::Result {
        //self.prom_active_players.set(len as f64);
        todo!("createsnake")
    }
}*/
impl Handler<UpdatePlayerCount> for Stats {
    type Result = ();

    fn handle(&mut self, msg: UpdatePlayerCount, _ctx: &mut Context<Self>) -> Self::Result {
        self.active_players.set(msg.0 as f64);
    }
}
impl Handler<ArenaConfig> for Stats {
    type Result = ();

    fn handle(&mut self, msg: ArenaConfig, _ctx: &mut Context<Self>) -> Self::Result {
        self.n_snakes.set(msg.n_snakes as f64);
    }
}
impl Handler<GetMetrics> for Stats {
    type Result = Metrics;

    fn handle(&mut self, _msg: GetMetrics, _ctx: &mut Self::Context) -> Self::Result {
        use prometheus::{
            Counter, CounterVec, Encoder, Gauge, GaugeVec, Opts, Registry, TextEncoder,
        };

        let mut buffer = Vec::<u8>::new();
        let encoder = TextEncoder::new();
        let metric_families = self.prometheus.gather();
        encoder.encode(&metric_families, &mut buffer).unwrap();

        // Output to the standard output.
        Metrics(
            String::from_utf8(
                buffer, //.clone()
            )
            .unwrap(),
        )
    }
}
impl Handler<StatsWebsocketUpgrade> for Stats {
    type Result = ();

    fn handle(&mut self, _msg: StatsWebsocketUpgrade, _ctx: &mut Self::Context) -> Self::Result {
        self.websocket_upgrade.inc()
    }
}
impl Handler<StatsNoWebsocketUpgrade> for Stats {
    type Result = ();

    fn handle(&mut self, _msg: StatsNoWebsocketUpgrade, _ctx: &mut Self::Context) -> Self::Result {
        self.no_websocket_upgrade.inc()
    }
}
impl Handler<UpdateSnakeStats> for Stats {
    type Result = ();

    fn handle(
        &mut self,
        UpdateSnakeStats {
            color,
            n_players,
            length,
        }: UpdateSnakeStats,
        _ctx: &mut Self::Context,
    ) -> Self::Result {
        if let Some((player_counter, len_counter)) = self.snake_players.get(&color) {
            player_counter.set(n_players as f64);
            len_counter.set(length as f64);
        } else {
            let opts = prometheus::Opts::new(
                "multiplayersnake_snake_n_players",
                "keeps track of how many users controll a given snake",
            )
            .const_label("color", color.clone());
            let player_counter = prometheus::Gauge::with_opts(opts).unwrap();
            player_counter.set(n_players as f64);

            let player_counter = Box::new(player_counter);

            self.prometheus.register(player_counter.clone()).unwrap();

            // Length counter
            let opts = prometheus::Opts::new(
                "multiplayersnake_snake_length",
                "keeps track of how long a given snake is",
            )
            .const_label("color", color.clone());
            let len_counter = prometheus::Gauge::with_opts(opts).unwrap();
            len_counter.set(length as f64);

            let len_counter = Box::new(len_counter);

            self.prometheus.register(len_counter.clone()).unwrap();

            self.snake_players
                .insert(color.clone(), (player_counter, len_counter));
        }
    }
}
impl Handler<RemoveSnakeStats> for Stats {
    type Result = ();

    fn handle(
        &mut self,
        RemoveSnakeStats { color }: RemoveSnakeStats,
        _ctx: &mut Self::Context,
    ) -> Self::Result {
        if let Some((c0, c1)) = self.snake_players.remove(&color) {
            self.prometheus
                .unregister(c0)
                .expect("unable to RemoveSnakeStats");
            self.prometheus
                .unregister(c1)
                .expect("unable to RemoveSnakeStats");
        }
    }
}

pub struct AutoScaler {
    arena: Option<ArenaAddr>,
    stats: Option<Addr<Stats>>,
    last_arena_config: Option<ArenaConfig>,
    active_players: usize,
    manual_override: bool,
}
impl AutoScaler {
    pub fn new() -> Addr<AutoScaler> {
        AutoScaler {
            manual_override: false,
            arena: None,
            stats: None,
            last_arena_config: None,
            active_players: 0,
        }
        .start()
    }
    fn calculate_new_target(&mut self) -> Option<()> {
        let ok = Some(());
        if self.arena.is_none() {
            debug!("AutoScaler.arena is None");
            return ok;
        }
        if self.stats.is_none() {
            debug!("AutoScaler.stats is None");
            return ok;
        }
        if self.manual_override {
            return ok;
        }

        let last = self.last_arena_config.as_ref()?;
        let last_n_snakes = last.n_snakes as isize;

        let mut n_players = self.active_players as isize;

        if n_players > 4 {
            n_players = 4.max(n_players / 2);
        }

        let n_snakes = 2.max(n_players).min(8);

        debug!(
            "AutoScaler::delta snakes: {} -> {}",
            last_n_snakes, n_snakes
        );

        if n_snakes != last_n_snakes {
            let (width, height) = self.calc_arena_size_from(n_snakes);
            self.arena.as_ref()?.do_send(UpdateArenaConfig {
                n_snakes: n_snakes as usize,
                height,
                width,
            })
        }

        ok
    }
    fn calc_arena_size_from(&self, n_snakes: isize) -> (isize, isize) {
        match n_snakes {
            0 | 1 => (10, 5),
            2 => (10, 6),
            3 => (12, 8),
            4 => (15, 10),
            5 => (18, 13),
            6 => (20, 15),
            7 => (26, 18),
            8 => (32, 24),
            _ => unreachable!(
                "AutoScaler::calc_arena_size_from(n_snakes = {}) out of defined range",
                n_snakes
            ),
        }
    }
}
impl Actor for AutoScaler {
    type Context = Context<Self>;
}
// Setup
impl Handler<SubscribeStats> for AutoScaler {
    type Result = ();

    fn handle(&mut self, msg: SubscribeStats, _ctx: &mut Self::Context) -> Self::Result {
        self.stats = Some(msg.0);
        if self.arena.is_some() {
            self.calculate_new_target();
        }
    }
}
impl Handler<SubscribeArena> for AutoScaler {
    type Result = ();

    fn handle(&mut self, msg: SubscribeArena, _ctx: &mut Self::Context) -> Self::Result {
        self.arena = Some(msg.0);
        if self.stats.is_some() {
            self.calculate_new_target();
        }
    }
}
// Interactive Messages
impl Handler<ArenaConfig> for AutoScaler {
    type Result = ();

    fn handle(&mut self, msg: ArenaConfig, _ctx: &mut Self::Context) -> Self::Result {
        self.last_arena_config = Some(msg);
        self.calculate_new_target();
    }
}
impl Handler<UpdatePlayerCount> for AutoScaler {
    type Result = ();

    fn handle(&mut self, msg: UpdatePlayerCount, _ctx: &mut Self::Context) -> Self::Result {
        self.active_players = msg.0;
        self.calculate_new_target();
    }
}

pub struct Arena {
    pub collision_rule: CrashRule,
    pub width: isize,
    pub height: isize,
    pub next_fruit: Pos2d,
    pub active_players: Vec<Addr<MyWs>>,
    pub snakes: Vec<SnakeInfo>,
    statistics: Addr<Stats>,
    auto_scaler: Addr<AutoScaler>,
}
pub type ArenaAddr = Addr<Arena>;
impl Arena {
    pub fn new(width: isize, height: isize, n_snakes: usize) -> (ArenaAddr, Addr<Stats>) {
        let auto_scaler = AutoScaler::new();
        let statistics = Stats::new(&auto_scaler);

        let arena = Arena {
            collision_rule: CrashRule::CrasherDies,
            //collision_rule: CrashRule::CrashedDies,
            //collision_rule: CrashRule::AllDie,
            height,
            width,
            next_fruit: Pos2d { x: 5, y: 4 },
            snakes: vec![],
            active_players: vec![],
            statistics: statistics.clone(),
            auto_scaler,
        }
        .start();

        arena.do_send(UpdateArenaConfig {
            width,
            height,
            n_snakes,
        });

        (arena, statistics)
    }
    fn broadcast_player_count(&mut self) {
        let len = self.active_players.len();

        self.statistics.do_send(UpdatePlayerCount(len));
        self.auto_scaler.do_send(UpdatePlayerCount(len));

        self.active_players
            .iter()
            .for_each(|p| p.do_send(UpdatePlayerCount(len)));
    }
    /// when flower is eaten and maybe for resizing
    fn broadcast_arena(&mut self) {
        //println!("Arena::broadcast_arena()");
        self.active_players.iter().for_each(|p| {
            p.do_send(UpdateFieldInfo {
                height: self.height,
                width: self.width,
                next_fruit: self.next_fruit,
            })
        });

        self.statistics.do_send(self.get_arena_config());
    }
    fn broadcast_snake_bodies(&mut self) {
        let snake_bodies = self
            .snakes
            .iter()
            .map(|s| &s.body)
            .cloned()
            .collect::<Vec<_>>();

        self.active_players
            .iter()
            .for_each(|p| p.do_send(SnakeBodies(snake_bodies.clone())));
    }

    fn tick(&mut self, context: &mut Context<Self>) {
        //println!("Arena::tick");

        if self.active_players.is_empty() == false {
            TimerFunc::new(TICK_INTERVAL, Self::tick).spawn(context);
            let collector = SnakesNextStepCollector::expecing(self.snakes.len(), context.address());
            self.snakes.iter().for_each(|s| {
                s.addr.do_send(ExecuteTick {
                    collector: collector.clone(),
                })
            });
        } else {
            info!("Arena::no active players -> sleeping");
        }
    }

    fn get_random_pos(&self, old: Option<Pos2d>) -> Pos2d {
        let mut rng = rand::thread_rng();

        loop {
            let (x, y) = (rng.gen_range(0..self.width), rng.gen_range(0..self.height));
            match old {
                None => {
                    break Pos2d { x, y };
                }
                Some(old) => {
                    if x != old.x && y != old.y {
                        break Pos2d { x, y };
                    }
                }
            }
        }
    }

    #[inline]
    fn get_arena_config(&self) -> ArenaConfig {
        ArenaConfig {
            width: self.width,
            height: self.height,
            n_snakes: self.snakes.len(),
            //max_ping: Duration::from_millis(0),
        }
    }
}
impl Actor for Arena {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.auto_scaler.do_send(SubscribeArena(ctx.address()));
    }
}
impl Handler<CreateSnake> for Arena {
    type Result = ();

    fn handle(
        &mut self,
        CreateSnake { body, heading }: CreateSnake,
        ctx: &mut Context<Self>,
    ) -> Self::Result {
        info!("Arena::CreateSnake");

        let mut rng = rand::thread_rng();
        let color = COLORS
            .iter()
            .filter(|potential_color| {
                self.snakes
                    .iter()
                    .map(|s| &s.body.color)
                    .any(|used| *potential_color == &*used)
                    == false
            })
            .collect::<Vec<_>>()
            .choose(&mut rng)
            .map(|c| c.to_string())
            .unwrap_or_else(|| {
                // generate random color
                let r: u8 = rng.gen_range(50..250);
                let g: u8 = rng.gen_range(50..250);
                let b: u8 = rng.gen_range(50..250);
                format!("#{:x}{:x}{:x}", r, g, b)
            });

        self.snakes.push(SnakeInfo {
            n_subscribers: 0,
            body: SnakeBody {
                color: color.clone(),
                heading: Heading::East,
                body: vec![],
            },
            addr: Snake {
                arena: ctx.address(),
                stats: self.statistics.clone(),
                subscribers: vec![],
                color,
                body,
                heading,
            }
            .start(),
        });

        let config = self.get_arena_config();
        self.statistics.do_send(config.clone());
        self.auto_scaler.do_send(config);
    }
}
impl Handler<SubscribeMyWs> for Arena {
    type Result = ();

    fn handle(&mut self, msg: SubscribeMyWs, ctx: &mut Context<Self>) -> Self::Result {
        let start_tick = self.active_players.is_empty();
        //println!("Arena::SubscribeMyWs {{ active_players.empty = {} }}", start_tick);
        let mut rng = rand::thread_rng();

        let addr = msg.0;
        addr.do_send(UpdateFieldInfo {
            width: self.width,
            height: self.height,
            next_fruit: self.next_fruit,
        });

        if self.active_players.contains(&addr) == false {
            self.active_players.push(addr.clone());
            self.broadcast_player_count();
        } else {
            info!("Arena::SubscribeMyWs -> Reassign");
        }

        let snake = match self.snakes.iter().filter(|si| si.n_subscribers == 0).next() {
            Some(si) => si,
            None => {
                self.snakes
                    .choose_weighted(&mut rng, |si| {
                        // Workaround because this panics
                        // 1.0 / si.n_subscribers as f64
                        MAX_WEIGHT
                            .checked_div(si.n_subscribers * 8)
                            .unwrap_or(MAX_WEIGHT)
                    })
                    .expect("Arena::SubscribeMyWs unable to select random snake")
            }
        };

        snake.addr.do_send(SubscribeMyWs(addr));

        if start_tick {
            self.tick(ctx);
        }
    }
}
impl Handler<UnsubscribeMyWs> for Arena {
    type Result = ();

    fn handle(&mut self, msg: UnsubscribeMyWs, _ctx: &mut Context<Self>) -> Self::Result {
        //println!("Arena::Removing MyWs");
        self.active_players.retain(|e| *e != msg.0);
        self.broadcast_player_count();

        self.snakes.iter().for_each(|s| s.addr.do_send(msg.clone()));
    }
}
impl Handler<UpdatePlayerCountOfSnake> for Arena {
    type Result = ();

    fn handle(
        &mut self,
        UpdatePlayerCountOfSnake {
            n_subscribers,
            color,
        }: UpdatePlayerCountOfSnake,
        _ctx: &mut Context<Self>,
    ) -> Self::Result {
        for si in &mut self.snakes {
            if si.body.color == color {
                si.n_subscribers = n_subscribers;
            }
        }
    }
}
impl Handler<CollectedSnakeBodies> for Arena {
    type Result = ();

    fn handle(&mut self, msg: CollectedSnakeBodies, _ctx: &mut Context<Self>) -> Self::Result {
        let mut reset_fruit = false;
        // Do the steps
        for mut snake in msg.0 {
            let local_snake = match self
                .snakes
                .iter_mut()
                .filter(|s| s.body.color == snake.color)
                .next()
            {
                Some(local_snake) => local_snake,
                None => {
                    error!("Arena::CollectedSnakeBodies unable to find Snake, skipping...");
                    continue;
                }
            };

            // handle wrap around
            if let Some(head) = snake.body.get_mut(0) {
                if head.x < 0 || head.y < 0 || head.x >= self.width || head.y >= self.height {
                    head.x = head.x.wrapping_rem_euclid(self.width);
                    head.y = head.y.wrapping_rem_euclid(self.height);
                    {
                        let head = head.clone();
                        local_snake.addr.do_send(OverwriteHead(head.clone()));
                    }
                }
            }

            if snake.body.is_empty() == false && snake.body[0] == self.next_fruit {
                reset_fruit = true;
            } else {
                snake.body.truncate(snake.body.len() - 1);
                local_snake.addr.do_send(ClipTail);
            }

            local_snake.body.heading = snake.heading;
            local_snake.body.body = snake.body;
        }

        let mut occupied_fields = DefaultHashMap::new(Vec::with_capacity(self.snakes.len()));
        for (nth, snake) in self.snakes.iter().enumerate() {
            snake.body.body.iter().for_each(|p| {
                occupied_fields[*p].push(nth);
            });
        }

        // detect crashes
        let mut crashed_snakes = vec![false; self.snakes.len()];
        for (i, snake) in self.snakes.iter().enumerate() {
            let head = match snake.body.body.get(0) {
                Some(head) => *head,
                None => {
                    error!("Arena::CollectedSnakeBodies snake.body has no head element");
                    continue;
                }
            };

            let collisions = &occupied_fields[head];
            if collisions.len() > 1 {
                match self.collision_rule {
                    CrashRule::AllDie => {
                        // all of these snakes die
                        for i in collisions {
                            crashed_snakes[*i] = true;
                        }
                    }
                    CrashRule::CrasherDies => {
                        crashed_snakes[i] = true;
                    }
                    CrashRule::CrashedDies => {
                        for o in collisions {
                            if *o != i {
                                crashed_snakes[*o] = true;
                            }
                        }
                    }
                }
            }
        }

        for (nth, snake) in self.snakes.iter().enumerate() {
            if crashed_snakes[nth] {
                let pos = self.get_random_pos(None);
                snake.addr.do_send(ResetAt(pos));
            }
        }

        self.broadcast_snake_bodies();
        if reset_fruit {
            self.next_fruit = self.get_random_pos(Some(self.next_fruit));
            self.broadcast_arena();
        }
    }
}
impl Handler<GetArenaConfig> for Arena {
    type Result = ArenaConfig;

    fn handle(&mut self, _msg: GetArenaConfig, _ctx: &mut Self::Context) -> Self::Result {
        self.get_arena_config()
    }
}
impl Handler<UpdateArenaConfig> for Arena {
    type Result = ArenaConfig;

    fn handle(&mut self, msg: UpdateArenaConfig, ctx: &mut Self::Context) -> Self::Result {
        let UpdateArenaConfig {
            width,
            height,
            n_snakes,
        } = msg;
        let mut update_arena = false;

        if width > 3 {
            self.width = width;
            update_arena = true;
        }
        if height > 3 {
            self.height = height;
            update_arena = true;
        }

        if self.next_fruit.x >= width || self.next_fruit.y >= height {
            self.next_fruit = self.get_random_pos(None);
        }

        let current_snakes = self.snakes.len();
        if n_snakes > current_snakes {
            for _ in current_snakes..n_snakes {
                ctx.address().do_send(CreateSnake {
                    body: vec![Pos2d { x: 0, y: 0 }],
                    heading: Heading::random(),
                });
            }
            update_arena = true;
        }
        if n_snakes < current_snakes {
            //todo!("reduce n _snakes");
            for snake in self.snakes.iter().skip(n_snakes).map(|t| &t.addr) {
                snake.do_send(RemoveSnake);
            }
            self.snakes.truncate(n_snakes);

            update_arena = true;
        }

        if update_arena {
            self.broadcast_arena();
        }

        self.get_arena_config()
    }
}

pub struct Snake {
    arena: ArenaAddr,
    subscribers: Vec<Addr<MyWs>>,
    stats: Addr<Stats>,
    color: SnakeColor,
    /// Head to tail
    body: Vec<Pos2d>,
    heading: Heading,
}
pub type SnakeAddr = Addr<Snake>;
impl Snake {
    fn update_stats(&mut self) {
        self.stats.do_send(UpdateSnakeStats {
            color: self.color.clone(),
            n_players: self.subscribers.len(),
            length: self.body.len(),
        });
        self.arena.do_send(UpdatePlayerCountOfSnake {
            n_subscribers: self.subscribers.len(),
            color: self.color.clone(),
        });
    }
}
impl Actor for Snake {
    type Context = Context<Self>;

    fn started(&mut self, _ctx: &mut Self::Context) {
        self.update_stats();
    }
    fn stopped(&mut self, _ctx: &mut Self::Context) {
        self.stats.do_send(RemoveSnakeStats {
            color: self.color.clone(),
        });
    }
}
impl Handler<SubscribeMyWs> for Snake {
    type Result = ();

    fn handle(&mut self, msg: SubscribeMyWs, ctx: &mut Context<Self>) -> Self::Result {
        msg.0.do_send(SubscribeSnake {
            snake: ctx.address(),
            color: self.color.clone(),
        });

        self.subscribers.push(msg.0);
        self.update_stats();
    }
}
impl Handler<UnsubscribeMyWs> for Snake {
    type Result = ();

    fn handle(&mut self, msg: UnsubscribeMyWs, _ctx: &mut Context<Self>) -> Self::Result {
        self.subscribers.retain(|e| *e != msg.0);
        self.update_stats();
    }
}
impl Handler<RemoveSnake> for Snake {
    type Result = ();

    fn handle(&mut self, _msg: RemoveSnake, _ctx: &mut Context<Self>) -> Self::Result {
        info!("Snake::RemoveSnake");
        for ws in &self.subscribers {
            self.arena.do_send(SubscribeMyWs(ws.clone()));
        }
    }
}
impl Handler<ExecuteTick> for Snake {
    //type Result = MessageResult<SnakeNextStep>;
    type Result = ();

    fn handle(&mut self, msg: ExecuteTick, ctx: &mut Context<Self>) -> Self::Result {
        //println!("Snake::ExecuteTick");

        let collector = DirectionCollector::expecing(
            self.subscribers.len(),
            ctx.address(),
            msg.collector.clone(),
        );

        for client in &self.subscribers {
            client.do_send(GetCurrentUserAction(collector.clone()));
        }
    }
}

impl Handler<CollectedDirection> for Snake {
    type Result = ();

    fn handle(&mut self, msg: CollectedDirection, _ctx: &mut Context<Self>) -> Self::Result {
        let next_action = msg.input;
        //println!("Snake::next_action = {:?}", next_action);

        let Pos2d { x, y } = self.body[0];
        let next_heading = match next_action {
            PlayerInput::Straight => self.heading,
            PlayerInput::Left => match self.heading {
                North => West,
                West => South,
                South => East,
                East => North,
            },
            PlayerInput::Right => match self.heading {
                North => East,
                East => South,
                South => West,
                West => North,
            },
        };

        let next_pos = match next_heading {
            North => Pos2d { x, y: y - 1 },
            South => Pos2d { x, y: y + 1 },
            West => Pos2d { x: x - 1, y },
            East => Pos2d { x: x + 1, y },
        };
        self.heading = next_heading;

        self.body.insert(0, next_pos);

        msg.arena_collector.do_send(SnakeBody {
            color: self.color.clone(),
            heading: next_heading,
            body: self.body.clone(),
        });
    }
}
impl Handler<ResetAt> for Snake {
    //type Result = MessageResult<SnakeNextStep>;
    type Result = ();

    fn handle(&mut self, msg: ResetAt, _ctx: &mut Context<Self>) -> Self::Result {
        //println!("Snake::ResetAt");
        self.body[0] = msg.0;
        self.body.truncate(1);
        self.update_stats();
    }
}
impl Handler<OverwriteHead> for Snake {
    //type Result = MessageResult<SnakeNextStep>;
    type Result = ();

    fn handle(&mut self, msg: OverwriteHead, _ctx: &mut Context<Self>) -> Self::Result {
        //println!("Snake::OverwriteHead");
        self.body[0] = msg.0;
    }
}
impl Handler<ClipTail> for Snake {
    //type Result = MessageResult<SnakeNextStep>;
    type Result = ();

    fn handle(&mut self, _msg: ClipTail, _ctx: &mut Context<Self>) -> Self::Result {
        //println!("Snake::ClipTail");
        self.body.truncate((self.body.len() - 1).max(2));
        self.update_stats();
    }
}

struct DirectionCollector {
    expected_values: usize,
    has_inactive_players: bool,
    snake: SnakeAddr,
    arena_collector: Addr<SnakesNextStepCollector>,
    left: usize,
    straight: usize,
    right: usize,
}
impl DirectionCollector {
    fn expecing(
        expected_values: usize,
        snake: SnakeAddr,
        arena_collector: Addr<SnakesNextStepCollector>,
    ) -> Addr<DirectionCollector> {
        DirectionCollector {
            expected_values,
            snake,
            arena_collector,
            has_inactive_players: false,
            left: 0,
            straight: 0,
            right: 0,
        }
        .start()
    }
    fn calc_next_action(left: usize, straight: usize, right: usize) -> PlayerInput {
        if left > straight && left > right {
            PlayerInput::Left
        } else {
            if right > straight {
                PlayerInput::Right
            } else {
                PlayerInput::Straight
            }
        }
    }
    fn choose_random_direction_add() -> (usize, usize, usize) {
        let rng = &mut rand::thread_rng();
        let (mut left, mut straight, mut right) = (0, 0, 0);

        let _ = [(&mut left, 1), (&mut straight, 6), (&mut right, 1)]
            .choose_weighted_mut(rng, |item| item.1)
            .map(|d| *d.0 = 1usize);
        //println!("chaos monkey: {:?}", (left, straight, right));

        (left, straight, right)
    }
    fn add_random_direction_vote(&mut self) {
        let (l, s, r) = Self::choose_random_direction_add();
        self.left = l;
        self.straight = s;
        self.right = r;
    }

    fn decide(&mut self, context: &mut Context<Self>) {
        if self.has_inactive_players {
            //println!("DirectionCollector::decide() has_inactive_players");
            if self.left + self.straight + self.right == 0 {
                if rand::random::<f64>() < RANDOM_ACTION_CANCHE_FOR_INACTIVE_SNAKES {
                    self.add_random_direction_vote();
                }
            }
        }
        let next_action = Self::calc_next_action(self.left, self.straight, self.right);
        //println!("DirectionCollector::decide -> {:?}", next_action);
        self.snake.do_send(CollectedDirection {
            input: next_action,
            arena_collector: self.arena_collector.clone(),
        });
        context.stop();
    }
    fn timeout(&mut self, context: &mut Context<Self>) {
        warn!("DirectionCollector::timeout");
        self.decide(context);
    }
}
impl Actor for DirectionCollector {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        if self.expected_values == 0 {
            self.add_random_direction_vote();

            self.decide(ctx);
        } else {
            // in case one of the MyWs disconnects during an election
            TimerFunc::new(TICK_INTERVAL / 10, Self::timeout).spawn(ctx);
        }
    }
}
impl Handler<MaybePlayerInput> for DirectionCollector {
    type Result = ();

    fn handle(&mut self, msg: MaybePlayerInput, ctx: &mut Self::Context) -> Self::Result {
        self.expected_values -= 1;
        match msg {
            MaybePlayerInput::None { for_n_rounds } => {
                if for_n_rounds > N_ROUNDS_BEFORE_IDLE_PLAYER {
                    self.has_inactive_players = true
                }
            }
            MaybePlayerInput::Some(input) => match input {
                PlayerInput::Left => self.left += 1,
                PlayerInput::Straight => self.straight += 1,
                PlayerInput::Right => self.right += 1,
            },
        }

        if self.expected_values == 0 {
            self.decide(ctx);
        }
    }
}
impl std::fmt::Debug for DirectionCollector {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DirectionCollector")
            .field("left", &self.left)
            .field("straight", &self.straight)
            .field("right", &self.right)
            .finish()
    }
}

struct SnakesNextStepCollector {
    expected_values: usize,
    arena: ArenaAddr,
    bodies: Vec<SnakeBody>,
}
impl SnakesNextStepCollector {
    fn expecing(expected_values: usize, arena: ArenaAddr) -> Addr<SnakesNextStepCollector> {
        //println!("SnakesNextStepCollector::expecting({}, ...)", expected_values);
        SnakesNextStepCollector {
            expected_values,
            arena,
            bodies: vec![],
        }
        .start()
    }
    fn decide(&mut self, context: &mut Context<Self>) {
        //println!("DirectionCollector::decide -> {:?}", next_action);
        let mut bodies = Vec::with_capacity(0);
        // Don't copy the contents of the Vector
        std::mem::swap(&mut bodies, &mut self.bodies);

        self.arena.do_send(CollectedSnakeBodies(bodies));
        context.stop();
    }
    fn timeout(&mut self, context: &mut Context<Self>) {
        println!("DirectionCollector::timeout");
        self.decide(context);
    }
}
impl Actor for SnakesNextStepCollector {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        if self.expected_values == 0 {
            self.decide(ctx);
        } else {
            // in case one of the MyWs disconnects during an election
            TimerFunc::new(TICK_INTERVAL / 10, Self::timeout).spawn(ctx);
        }
    }
}
impl Handler<SnakeBody> for SnakesNextStepCollector {
    type Result = ();

    fn handle(&mut self, msg: SnakeBody, ctx: &mut Self::Context) -> Self::Result {
        self.expected_values -= 1;
        self.bodies.push(msg);

        if self.expected_values == 0 {
            self.decide(ctx);
        }
    }
}

/// Define HTTP actor
pub struct MyWs {
    arena: ArenaAddr,
    snake: Option<SnakeAddr>,
    current_input: MaybePlayerInput,
    ping_start: Instant,
}
impl MyWs {
    pub fn new(arena: ArenaAddr) -> MyWs {
        MyWs {
            arena,
            snake: None,
            current_input: MaybePlayerInput::None { for_n_rounds: 0 },
            ping_start: Instant::now(),
        }
    }
    fn send_to_client(&mut self, msg: ClientCommand, ctx: &mut WebsocketContext<Self>) {
        ctx.text(serde_json::to_string(&msg).expect("unable to serialize UpdateFieldInfo"))
    }
    fn ping_client(&mut self, context: &mut WebsocketContext<Self>) {
        context.ping(&PING_PONG_DATA);
        self.ping_start = Instant::now();
        TimerFunc::new(TICK_INTERVAL * 5, Self::ping_client).spawn(context);
    }
}
impl Actor for MyWs {
    type Context = WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let addr = ctx.address();
        self.arena.do_send(SubscribeMyWs(addr));
        self.ping_client(ctx);
    }

    fn stopped(&mut self, ctx: &mut Self::Context) {
        self.arena.do_send(UnsubscribeMyWs(ctx.address()));
        if let Some(snake) = &self.snake {
            snake.do_send(UnsubscribeMyWs(ctx.address()));
        }
    }
}
impl Handler<UpdatePlayerCount> for MyWs {
    type Result = ();

    fn handle(&mut self, msg: UpdatePlayerCount, ctx: &mut Self::Context) -> Self::Result {
        self.send_to_client(ClientCommand::UpdatePlayerCount(msg), ctx)
    }
}
impl Handler<UpdateFieldInfo> for MyWs {
    type Result = ();

    fn handle(&mut self, msg: UpdateFieldInfo, ctx: &mut Self::Context) -> Self::Result {
        self.send_to_client(ClientCommand::UpdateFieldInfo(msg), ctx)
    }
}
impl Handler<SnakeBodies> for MyWs {
    type Result = ();

    fn handle(&mut self, msg: SnakeBodies, ctx: &mut Self::Context) -> Self::Result {
        self.send_to_client(ClientCommand::UpdateSnakes(msg), ctx)
    }
}
impl Handler<GetCurrentUserAction> for MyWs {
    type Result = ();

    fn handle(&mut self, msg: GetCurrentUserAction, _ctx: &mut Self::Context) -> Self::Result {
        use MaybePlayerInput::{None, Some};
        let (current_input, next_input) = match &self.current_input {
            None { for_n_rounds } => (
                None {
                    for_n_rounds: *for_n_rounds,
                },
                None {
                    for_n_rounds: for_n_rounds.saturating_add(1),
                },
            ),
            Some(input) => (Some(input.clone()), None { for_n_rounds: 0 }),
        };
        self.current_input = next_input;
        msg.0.do_send(current_input);
    }
}
//*
impl Handler<SubscribeSnake> for MyWs {
    type Result = ();

    fn handle(&mut self, msg: SubscribeSnake, ctx: &mut WebsocketContext<Self>) -> Self::Result {
        self.snake = Some(msg.snake);
        self.send_to_client(ClientCommand::UpdateControllColor(msg.color), ctx);
    }
}
// */
/// Handler for ws::Message message
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for MyWs {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        use ws::Message;

        match msg {
            Ok(Message::Ping(msg)) => ctx.pong(&msg),
            Ok(Message::Pong(msg)) => {
                if *msg != PING_PONG_DATA {
                    println!("MyWs::StreamHandler invalid Pong data: {:?}", msg);
                }
                let elapsed = self.ping_start.elapsed();
                //println!("MyWs::Pong elapsed = {:?}", elapsed);
                let ms = elapsed.as_millis() as usize;
                // Warn user when the delay becomes half the Tick
                let msg = if elapsed > TICK_INTERVAL / 2 {
                    PingTime::NetworkTooSlow { ms }
                } else {
                    PingTime::Ping { ms }
                };
                self.send_to_client(ClientCommand::PingTime(msg), ctx);
            }
            Ok(Message::Text(text)) => {
                let input: PlayerInput = match serde_json::from_str(&text) {
                    Ok(input) => input,
                    Err(_e) => {
                        return self.send_to_client(ClientCommand::InvalidInput(text), ctx);
                    }
                };
                self.current_input = MaybePlayerInput::Some(input);
            }
            _ => (),
        }
    }
}

pub(crate) fn debug_messages() {
    let t = |s| println!("\n ==== {} ==== ", s);
    fn p<S: Serialize>(o: S) {
        println!("    > {}", serde_json::to_string(&o).unwrap());
    }
    fn d<T: std::fmt::Debug>(s: T) {
        println!("    < {:?}", s);
    }
    use serde_json::from_str as fs;
    //use game::{ClientCommand,UpdateFieldInfo,UpdatePlayerCount,SnakeBody,PlayerInput,};

    t("ClientCommand");
    p(ClientCommand::UpdateFieldInfo(UpdateFieldInfo {
        height: 2,
        width: 3,
        next_fruit: Pos2d { x: 1, y: 2 },
    }));
    p(ClientCommand::UpdatePlayerCount(UpdatePlayerCount::new(42)));
    p(ClientCommand::UpdateControllColor("red".to_string()));
    p(ClientCommand::UpdateSnakes(SnakeBodies(vec![SnakeBody {
        color: "red".to_string(),
        heading: Heading::North,
        body: vec![Pos2d { x: 0, y: 0 }, Pos2d { x: 1, y: 1 }],
    }])));
    p(ClientCommand::PingTime(PingTime::NetworkTooSlow { ms: 42 }));
    p(ClientCommand::PingTime(PingTime::Ping { ms: 42 }));
    p(ClientCommand::InvalidInput("bla".to_string()));

    t("PlayerInput");
    p(PlayerInput::Left);
    d(fs::<PlayerInput>("{\"PlayerInput\":\"Left\"}"));
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn dc_decide_left() {
        let (left, straight, right) = (1, 0, 0);
        let next = DirectionCollector::calc_next_action(left, straight, right);

        assert_eq!(PlayerInput::Left, next);
    }
    #[test]
    fn dc_decide_straigt() {
        let (left, straight, right) = (0, 1, 0);
        let next = DirectionCollector::calc_next_action(left, straight, right);

        assert_eq!(PlayerInput::Straight, next);
    }
    #[test]
    fn dc_decide_right() {
        let (left, straight, right) = (0, 0, 1);
        let next = DirectionCollector::calc_next_action(left, straight, right);

        assert_eq!(PlayerInput::Right, next);
    }
    #[test]
    fn dc_decide_noinput() {
        let (left, straight, right) = (0, 0, 0);
        let next = DirectionCollector::calc_next_action(left, straight, right);

        assert_eq!(PlayerInput::Straight, next);
    }

    #[test]
    fn choose_random_direction_add() {
        let (l, s, r) = DirectionCollector::choose_random_direction_add();
        assert_eq!(1, l + s + r);
    }
}
